package com.software.netty.client;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 描述：
 *
 * @ClassName NettyClientHandler
 * @Author 徐旭
 * @Date 2018/11/8 13:51
 * @Version 1.0
 */
public class NettyClientHandler extends SimpleChannelInboundHandler<String> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        System.out.println("收到服务端消息:" + msg);
    }
}
