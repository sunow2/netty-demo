package com.software.netty.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

/**
 * 描述：
 *
 * @ClassName NettyClient
 * @Author 徐旭
 * @Date 2018/11/8 13:32
 * @Version 1.0
 */
@Slf4j
public class NettyClient {

    /**
     * 主机
     */
    private String host;

    /**
     * 端口号
     */
    private int port;

    public NettyClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    /**
     * 连接方法
     */
    public void connect() {
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group).channel(NioSocketChannel.class);
            bootstrap.option(ChannelOption.TCP_NODELAY, true);
            bootstrap.handler(new NettyClientInitializer());

            Channel channel = bootstrap.connect(host, port).sync().channel();
            // 发送字符串
            String msg = "{\"name\":\"admin\", \"age\":27}\n";
            channel.writeAndFlush(msg);
            channel.closeFuture().sync();
        } catch (InterruptedException e) {
            log.error("连接失败:" + e.getMessage());
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }
    }

    /**
     * 测试入口
     * @param args
     */
    public static void main(String... args) {
        String host = "127.0.0.1";
        int port = 8088;
        System.out.println("客户端尝试连接");
        NettyClient nettyClient = new NettyClient(host, port);
        nettyClient.connect();

    }
}
